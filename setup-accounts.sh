#! /bin/bash

completium-cli generate account as daniel --with-tezos-client
completium-cli generate account as eddy   --with-tezos-client
completium-cli generate account as flo    --with-tezos-client
completium-cli generate account as gary   --with-tezos-client
completium-cli generate account as hugo   --with-tezos-client
completium-cli generate account as ian    --with-tezos-client
completium-cli generate account as jacky  --with-tezos-client
